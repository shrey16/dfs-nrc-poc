# NRC Lambdas

Lambdas application written in Java for NRC Modernization project

```bash
.
├── nrc-lambdas
│   ├── build.gradle                                   <-- Java Dependencies
│   ├── gradle                                         <-- Gradle related Boilerplate
│   │   └── wrapper
│   │       ├── gradle-wrapper.jar
│   │       └── gradle-wrapper.properties
│   ├── gradlew                                        <-- Linux/Mac Gradle Wrapper
│   ├── gradlew.bat                                    <-- Windows Gradle Wrapper
│   └── src
│       ├── main
│       │   └── java
│       │       └── com.dfs.nrc
|       |           └── lambda                          <-- Source code for a lambda function
│       │               └── DecodeLambda.java           <-- Lambda function code
│       └── test                                        <-- Unit tests
│           └── java
│               └── com.dfs.nrc
│                   └── lambda
│                       └── DecodeLambdaTest.java
├── README.md                                          <-- This instructions file
```

## Requirements

* AWS CLI already configured with Administrator permission
* [Java SE Development Kit 8 installed](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Gradle](https://gradle.org/install/)

## Setup process

### Installing dependencies

```bash
gradle wrapper (use wrapper version from DFS)
./gradlew build (will build application and run all tests)
```

## Testing

We use `JUnit` for testing our code and you can simply run the following command to run our tests:

```bash
./gradlew test
```
