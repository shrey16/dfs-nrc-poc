package com.dfs.nrc.config;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.enums.WorkflowStep;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class TestConfig {

    /**
     * Creates a SQS Queue on localstack AWS Cloud Stack
     *
     * @param sqsClient SQS Client
     * @param queueName Name of the SQS Queue
     * @return Queue URL
     */
    public static String createQueue(AmazonSQS sqsClient, String queueName) {
        CreateQueueRequest create_request = new CreateQueueRequest(queueName)
            .addAttributesEntry("DelaySeconds", "0")
            .addAttributesEntry("MessageRetentionPeriod", "86400");
        try {
            sqsClient.createQueue(create_request);
        } catch (AmazonSQSException e) {
            if (!e.getErrorCode().equals("QueueAlreadyExists")) {
                throw e;
            }
        }
        return sqsClient.getQueueUrl(queueName).getQueueUrl();
    }

    /**
     * Creates a Send Message Request object
     *
     * @param queueUrl URL of the SQS Message
     * @param queueMsg Message you want to push to the queue
     * @return Send Message Request object
     */
    public static SendMessageRequest createMsgRequest(String queueUrl, String queueMsg) {
        return new SendMessageRequest()
            .withQueueUrl(queueUrl)
            .withMessageBody(queueMsg)
            .withDelaySeconds(0);
    }

    /**
     * Creates a SQS event for testing
     *
     * @param workflowStep Workflow step the event should be created for
     * @param fileName     File name to use for testing
     * @return Returns an SQS Event object
     */
    public static SQSEvent createSQSEvent(String fileName, WorkflowStep workflowStep, @Nullable WorkflowResult workflowResult) {
        WorkflowEvent workflowEvent = new WorkflowEvent(fileName);
        workflowEvent.setWorkflowStep(workflowStep);
        if (workflowResult != null) {
            workflowEvent.setWorkflowResult(workflowResult);
        }

        SQSEvent.SQSMessage message = new SQSEvent.SQSMessage();
        message.setBody(WorkflowEventMapper.messageBodyFromDomain(workflowEvent));
        List<SQSEvent.SQSMessage> messages = new ArrayList<>();
        messages.add(message);
        SQSEvent event = new SQSEvent();

        event.setRecords(messages);
        return event;
    }

    /**
     * Will get and return the SQS Event message as a Workflow Event object
     *
     * @param sqsEvent The SQS event
     * @return Message as a WorkflowEvent object
     */

    public static WorkflowEvent getSQSMessage(SQSEvent sqsEvent) {
        return WorkflowEventMapper.domainFromMessageBody(sqsEvent.getRecords().get(0).getBody());
    }

    /**
     * Will get and return the SQS Result message as a Workflow Event object
     *
     * @param sqsMessageResult MessageResult
     * @return Message as a WorkflowEvent object
     */
    public static WorkflowEvent getSQSMessage(ReceiveMessageResult sqsMessageResult) {
        return WorkflowEventMapper.domainFromMessageBody(sqsMessageResult.getMessages().get(0).getBody());
    }

    /**
     * Creates an SNS topic
     *
     * @param snsClient SNS Client
     * @param topicName Name of the SNS Topic
     * @return Topic ARN
     */
    public static String createSNSTopic(AmazonSNS snsClient, String topicName) {
        final CreateTopicRequest createTopicRequest = new CreateTopicRequest(topicName);
        final CreateTopicResult createTopicResponse = snsClient.createTopic(createTopicRequest);
        return createTopicResponse.getTopicArn();
    }

    /**
     * Published an SNS message
     *
     * @param snsClient  SNS Client
     * @param topicArn   Topic ARN of the SNS Topic
     * @param snsSubject SNS Message Subject
     * @param snsBody    SNS Message Body
     */
    public static void publishSNSMsg(AmazonSNS snsClient, String topicArn, String snsSubject, String snsBody) {
        PublishRequest SNSRequest = new PublishRequest(topicArn, snsBody).withSubject(snsSubject);
        snsClient.publish(SNSRequest);
    }

    /**
     * Creates a Dynamo DB table
     *
     * @param dynamoClient DynamoDB Connection
     * @param tableName    table name
     */
    public static void createTable(AmazonDynamoDB dynamoClient, String tableName) {
        DynamoDB dynamoDBConn = new DynamoDB(dynamoClient);
        try {
            List<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
            attributeDefinitions.add(new AttributeDefinition().withAttributeName("uuid").withAttributeType("S"));

            List<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
            keySchema.add(new KeySchemaElement().withAttributeName("uuid").withKeyType(KeyType.HASH));

            CreateTableRequest request = new CreateTableRequest()
                .withTableName(tableName)
                .withKeySchema(keySchema)
                .withAttributeDefinitions(attributeDefinitions)
                .withProvisionedThroughput(new ProvisionedThroughput()
                    .withReadCapacityUnits(5L)
                    .withWriteCapacityUnits(6L));

            Table dynamoTable = dynamoDBConn.createTable(request);
            dynamoTable.waitForActive();
        } catch (Exception e) {
            System.err.println("CreateTable request failed for " + tableName);
            System.err.println(e.getMessage());
        }
    }

}
