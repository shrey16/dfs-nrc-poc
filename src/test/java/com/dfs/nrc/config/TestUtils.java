package com.dfs.nrc.config;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.util.IOUtils;
import com.dfs.nrc.mapper.DateUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper utilities for testing Lambda functions.
 */

@SuppressWarnings("deprecation")
public class TestUtils {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final ObjectMapper snsEventMapper = new ObjectMapper();

    static {
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);

        snsEventMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        snsEventMapper.setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE);
    }

    /**
     * Helper method that parses a JSON object from a resource on the classpath
     * as an instance of the provided type.
     *
     * @param resource the path to the resource (relative to this class)
     * @param clazz    the type to parse the JSON into
     */
    public static <T> T parse(String resource, Class<T> clazz) {
        String resourceFile = ClassLoader.getSystemResource(resource).getFile();
        File file = new File(resourceFile);
        InputStream stream;

        try {
            stream = new FileInputStream(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            if (clazz == S3Event.class) {
                String json = IOUtils.toString(stream);
                S3EventNotification event = S3EventNotification.parseJson(json);

                @SuppressWarnings("unchecked")
                T result = (T) new S3Event(event.getRecords());
                return result;

            } else if (clazz == SNSEvent.class) {
                return snsEventMapper.readValue(stream, clazz);
            } else if (clazz == String.class) {
                @SuppressWarnings("unchecked")
                T result = (T) IOUtils.toString(stream);
                return result;
            } else {
                return mapper.readValue(stream, clazz);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static Long setDate(@Nullable Integer addDays) {
        Calendar today = Calendar.getInstance();
        if (addDays != null) {
            today.add(Calendar.DATE, addDays);
        }
        return today.getTimeInMillis();
    }

    public static String constructFileName(Date fileDate, String vendorCode) {
        return "VERF" + vendorCode + DateUtil.stringFromDate(fileDate) + ".xls";
    }

    public static String extractFileName(String file) {
        return file.substring(0, file.indexOf('.'));
    }
}
