package com.dfs.nrc.util;

import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.enums.WorkflowStep;
import org.junit.Assert;
import org.junit.Test;

public class WorkflowProcessUtilTest {
    @Test
    public void nextStepSuccess() {
        WorkflowEvent event = new WorkflowEvent("");
        event.setWorkflowStep(WorkflowStep.TRIGGER);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.DECODE);

        event.setWorkflowStep(WorkflowStep.DECODE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.VALIDATE);

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.OUTPUT);

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);
    }

    @Test
    public void nextStepFailure() {
        WorkflowEvent event = new WorkflowEvent("");
        event.setWorkflowStep(WorkflowStep.TRIGGER);
        event.setWorkflowResult(WorkflowResult.FAILURE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);

        event.setWorkflowStep(WorkflowStep.DECODE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.NOTIFY);
    }

    @Test
    public void nextStepError() {
        WorkflowEvent event = new WorkflowEvent("");
        event.setWorkflowStep(WorkflowStep.TRIGGER);
        event.setWorkflowResult(WorkflowResult.ERROR);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);

        event.setWorkflowStep(WorkflowStep.DECODE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(WorkflowProcessUtil.nextStep(event), WorkflowStep.ERROR);
    }

    @Test
    public void queueNameSuccess() {
        WorkflowEvent event = new WorkflowEvent();
        event.setWorkflowStep(WorkflowStep.DECODE);
        event.setWorkflowResult(WorkflowResult.SUCCESS);
        Assert.assertEquals(AppConfiguration.DECODE_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(AppConfiguration.VALIDATE_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(AppConfiguration.OUTPUT_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.ERROR);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));
    }

    @Test
    public void queueNameFailure() {
        WorkflowEvent event = new WorkflowEvent();
        event.setWorkflowStep(WorkflowStep.DECODE);
        event.setWorkflowResult(WorkflowResult.FAILURE);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.ERROR);
        Assert.assertEquals(AppConfiguration.NOTIFY_QUEUE, WorkflowProcessUtil.queueName(event));
    }

    @Test
    public void queueNameError() {
        WorkflowEvent event = new WorkflowEvent("");
        event.setWorkflowStep(WorkflowStep.TRIGGER);
        event.setWorkflowResult(WorkflowResult.ERROR);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.VALIDATE);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.NOTIFY);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.OUTPUT);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));

        event.setWorkflowStep(WorkflowStep.ERROR);
        Assert.assertEquals(AppConfiguration.ERROR_QUEUE, WorkflowProcessUtil.queueName(event));
    }
}
