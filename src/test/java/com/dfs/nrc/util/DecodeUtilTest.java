package com.dfs.nrc.util;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.enums.DecodeRule;
import com.dfs.nrc.generator.VendorFileGenerator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class DecodeUtilTest {

    @Ignore
    @Test
    public void decodeResultsInvalidFileName() {
        VendorFile file = VendorFileGenerator.generateCustom("", "", "", null, Collections.emptyList(), Collections.emptyList());
        List<DecodeRule> results = DecodeUtil.decodeFile(file);

        Assert.assertEquals(1, results.size());
        Assert.assertEquals(DecodeRule.FILE_NAME, results.get(0));
    }

    @Ignore
    @Test
    public void decodeResultsInvalidDate() {
        VendorFile file = new VendorFile();
        String todayDate = new SimpleDateFormat("MMddyyyy").format(Calendar.getInstance().getTime());
        file.setFileName("VERFCODE" + todayDate + ".xls");
        Assert.assertEquals(DecodeUtil.decodeFile(file), Collections.EMPTY_LIST);

        file.setFileName("VERFCODE03072019.xls");
        Assert.assertEquals(DecodeUtil.decodeFile(file), Arrays.asList(DecodeRule.FILE_DATE));
    }

}
