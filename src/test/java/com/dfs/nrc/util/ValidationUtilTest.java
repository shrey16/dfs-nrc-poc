package com.dfs.nrc.util;

import com.dfs.nrc.domain.AccountSummary;
import com.dfs.nrc.domain.RecallAccount;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.enums.ValidationRule;
import com.dfs.nrc.generator.AccountSummaryGenerator;
import com.dfs.nrc.generator.VendorFileGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ValidationUtilTest {
    @Test
    public void decodeRules() {
        int numberOfRules = 2;
        VendorFile file = VendorFileGenerator.generate();
        Assert.assertEquals(numberOfRules, ValidationUtil.getRules(file).size());
    }

    @Test
    public void decodeResultsAreValid() {
        List<AccountSummary> summary = Arrays.asList(
                AccountSummaryGenerator.generate()
        );

        VendorFile file = VendorFileGenerator.generate();
        file.setSummaryList(summary);

        List<ValidationRule> results = ValidationUtil.validateFile(file);
        Assert.assertEquals(0, results.size());
    }

    @Test
    public void decodeResultsFileName() {
        VendorFile file = VendorFileGenerator.generateCustom("", "", "", null, Collections.emptyList(), Collections.emptyList());
        List<ValidationRule> results = ValidationUtil.validateFile(file);

        Assert.assertEquals(1, results.size());
        Assert.assertEquals(ValidationRule.FILE_NAME, results.get(0));
    }

    @Test
    public void decodeResultsInvalidMultiple() {
        List<RecallAccount> recallAccounts = Collections.emptyList();
        List<AccountSummary> summaryList = Arrays.asList(
                AccountSummaryGenerator.generate()
        );

        List<ValidationRule> results = ValidationUtil.validateFile(VendorFileGenerator.generateCustom("", "", "", null, recallAccounts, summaryList));
        Assert.assertEquals(2, results.size());
        Assert.assertEquals(ValidationRule.FILE_NAME, results.get(0));
        Assert.assertEquals(ValidationRule.ACCOUNTS, results.get(1));
    }
}
