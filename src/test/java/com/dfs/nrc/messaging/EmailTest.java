package com.dfs.nrc.messaging;

import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.config.TestUtils;
import com.dfs.nrc.domain.EmailMessage;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.DecodeRule;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.repository.file.FileRepository;
import com.dfs.nrc.repository.file.FileRepositoryImpl;
import com.dfs.nrc.service.template.TemplateService;
import com.dfs.nrc.service.template.TemplateServiceImpl;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;

public class EmailTest {

    // TODO: Mock TemplateService
    TemplateService templateService = new TemplateServiceImpl();

    @Test
    @Ignore
    public void EmailMessageSuccessFromS3TemplateTest() {
        VendorFile vendorFile = new VendorFile();
        vendorFile.setFileName("VERFCODE03072019.xls");
        vendorFile.setFileDate(Calendar.getInstance().getTime());
        WorkflowEvent event = new WorkflowEvent();
        event.setWorkflowResult(WorkflowResult.SUCCESS);
        FileRepository fileRepository = new FileRepositoryImpl(AmazonS3ClientBuilder.defaultClient(), AppConfiguration.EMAIL_TEMPLATES_BUCKET);
        EmailMessage msg = new EmailMessage("noreply@discover.com","david.durkin@slalom.com", "Success");
        msg.setContent(this.templateService.generateEmailMessageFromProperTemplate(vendorFile, event));
        System.out.println(msg.getContent());
    }

    @Test
    @Ignore
    public void EmailMessageFailureWithErrorFromS3TemplateTest() {
        VendorFile vendorFile = new VendorFile();
        vendorFile.setFileName(TestUtils.constructFileName(Calendar.getInstance().getTime(), "TOOLONG"));
        vendorFile.setDecodeErrors(Arrays.asList(DecodeRule.FILE_NAME));
        vendorFile.setFileDate(Calendar.getInstance().getTime());
        WorkflowEvent event = new WorkflowEvent();
        event.setWorkflowResult(WorkflowResult.FAILURE);
        FileRepository fileRepository = new FileRepositoryImpl(AmazonS3ClientBuilder.defaultClient(), AppConfiguration.EMAIL_TEMPLATES_BUCKET);
        EmailMessage msg = new EmailMessage("noreply@discover.com","david.durkin@slalom.com", "Verification File Falure");
        msg.setContent(this.templateService.generateEmailMessageFromProperTemplate(vendorFile, event));
        System.out.println(msg.getContent());
    }
}
