package com.dfs.nrc.mapper;

import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.ProcessType;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.enums.WorkflowStep;
import org.junit.Assert;
import org.junit.Test;

public class WorkflowEventMapperTest {
    private String jsonString(String uuid, String fileName, String processType, String step, String result) {
        return String.format("{\"uuid\":\"%s\",\"fileName\":\"%s\",\"processType\":\"%s\",\"workflowStep\":\"%s\",\"workflowResult\":\"%s\"}", uuid, fileName, processType, step, result);
    }

    @Test
    public void messageBodyFromDomain() {
        WorkflowEvent event = new WorkflowEvent("abdefg@gmail.com");
        String result = WorkflowEventMapper.messageBodyFromDomain(event);
        String uuid = event.getUuid();
        String expected = jsonString(event.getUuid(), event.getFileName(), event.getProcessType().name(), event.getWorkflowStep().name(), event.getWorkflowResult().name());

        Assert.assertEquals(expected, result);
    }

    @Test
    public void domainFromMessageBodySuccess() {
        String fileName = "abcde@gmail.com";
        WorkflowStep step = WorkflowStep.DECODE;
        WorkflowResult workflowResult = WorkflowResult.SUCCESS;
        ProcessType processType = ProcessType.AIF_MNT;
        String jsonString = jsonString("", fileName, processType.name(), step.name(), workflowResult.name());
        WorkflowEvent result = WorkflowEventMapper.domainFromMessageBody(jsonString);

        WorkflowEvent event = new WorkflowEvent(fileName);
        event.setWorkflowStep(step);
        event.setWorkflowResult(workflowResult);

        Assert.assertEquals(event.getFileName(), result.getFileName());
        Assert.assertEquals(event.getWorkflowStep(), result.getWorkflowStep());
    }

    @Test(expected = RuntimeException.class)
    public void domainFromMessageBodyFailure() {
        String filename = "";
        String workflowStep = "Not_ASTEP";


        String jsonString = jsonString("", filename, "", workflowStep, "");
        WorkflowEventMapper.domainFromMessageBody(jsonString);
    }
}
