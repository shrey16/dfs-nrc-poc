package com.dfs.nrc.generator;

import com.dfs.nrc.domain.RecallAccount;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class RecallAccountGenerator {
    private static Random random = new Random();

    public static RecallAccount generate() {
        Date fileDate = new Date();
        return new RecallAccount(random.toString(), random.toString(), fileDate);
    }

    public static RecallAccount generateCustom(@Nullable String accountNumber, @Nullable String workgroup, @Nullable Date fileDate) {
        if (accountNumber == null) accountNumber = random.toString();
        if (workgroup == null) workgroup = UUID.randomUUID().toString();
        if (fileDate == null) fileDate = new Date();
        return new RecallAccount(accountNumber, workgroup, fileDate);
    }
}

