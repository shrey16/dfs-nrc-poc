package com.dfs.nrc.generator;

import com.dfs.nrc.domain.AccountSummary;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Random;

public class AccountSummaryGenerator {
    private static Random random = new Random();

    public static AccountSummary generate() {
        return new AccountSummary(
            random.toString(),
            random.toString(),
            random.toString(),
            random.toString(),
            new Date(),
            new Date(),
            random.toString(),
            Math.abs(random.nextInt()),
            Math.abs(random.nextInt()),
            Math.abs(random.nextDouble()),
            Math.abs(random.nextInt()),
            Math.abs(random.nextDouble()));
    }

    public static AccountSummary generateCustom(
        @Nullable String vendorName,
        @Nullable String personVerifying,
        @Nullable String phone,
        @Nullable String email,
        @Nullable Date todaysDate,
        @Nullable Date fileDate,
        @Nullable String workGroup,
        @Nullable Integer accountsRecalled,
        @Nullable Integer accountsPlaced,
        @Nullable Double totalBalanceNewAccounts,
        @Nullable Integer totalAccountsReassigned,
        @Nullable Double totalBalanceReassigned) {
        if (vendorName == null) vendorName = random.toString();
        if (personVerifying == null) personVerifying = random.toString();
        if (phone == null) phone = random.toString();
        if (email == null) email = random.toString();
        if (todaysDate == null) todaysDate = new Date();
        if (fileDate == null) fileDate = new Date();
        if (workGroup == null) workGroup = random.toString();
        if (accountsPlaced == null) accountsPlaced = Math.abs(random.nextInt());
        if (accountsRecalled == null) accountsRecalled = Math.abs(random.nextInt());
        if (totalBalanceNewAccounts == null) totalBalanceNewAccounts = Math.abs(random.nextDouble());
        if (totalAccountsReassigned == null) totalAccountsReassigned = Math.abs(random.nextInt());
        if (totalBalanceReassigned == null) totalBalanceReassigned = Math.abs(random.nextDouble());
        return new AccountSummary(
            vendorName,
            personVerifying,
            phone,
            email,
            todaysDate,
            fileDate,
            workGroup,
            accountsRecalled,
            accountsPlaced,
            totalBalanceNewAccounts,
            totalAccountsReassigned,
            totalBalanceReassigned);
    }
}
