package com.dfs.nrc.generator;

import com.dfs.nrc.domain.AccountSummary;
import com.dfs.nrc.domain.RecallAccount;
import com.dfs.nrc.domain.VendorFile;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class VendorFileGenerator {
    private static Random random = new Random();

    public static VendorFile generate() {
        List<RecallAccount> accounts = Arrays.asList(RecallAccountGenerator.generate());
        List<AccountSummary> summaryList = Arrays.asList(AccountSummaryGenerator.generate());
        String vendorCode = random.toString();
        String fileName = random.toString();
        String fileExtension = random.toString();
        Date fileDate = new Date();
        return new VendorFile(vendorCode, fileName, fileExtension, fileDate, accounts, summaryList);
    }

    public static VendorFile generateCustom(String vendorCode, String fileName, String fileExtension, Date fileDate, List<RecallAccount> recallAccountList, List<AccountSummary> summaryList) {
        return new VendorFile(vendorCode, fileName, fileExtension, fileDate, recallAccountList, summaryList);
    }
}
