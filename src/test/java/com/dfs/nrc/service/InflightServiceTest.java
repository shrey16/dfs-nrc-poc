package com.dfs.nrc.service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.AccountSummary;
import com.dfs.nrc.domain.RecallAccount;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.generator.AccountSummaryGenerator;
import com.dfs.nrc.generator.RecallAccountGenerator;
import com.dfs.nrc.repository.inflight.InflightRepository;
import com.dfs.nrc.repository.inflight.InflightRepositoryImpl;
import com.dfs.nrc.service.inflight.InflightService;
import com.dfs.nrc.service.inflight.InflightServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class InflightServiceTest {
    private static AmazonDynamoDB dynamoDB;

    @Before
    public void setup() {
    }

    @After
    public void cleanup() {
        dynamoDB.deleteTable(AppConfiguration.INFLIGHT_TABLE);
    }

    @Ignore
    @Test
    public void saveAndLoadTest() {
        InflightRepository inflightRepository = new InflightRepositoryImpl(dynamoDB);
        InflightService inflightService = new InflightServiceImpl(inflightRepository);

        List<RecallAccount> recallAccounts = Arrays.asList(
            RecallAccountGenerator.generate(),
            RecallAccountGenerator.generate()
        );

        List<AccountSummary> summaryList = Arrays.asList(
            AccountSummaryGenerator.generate(),
            AccountSummaryGenerator.generate()
        );

        String uuid = UUID.randomUUID().toString();
        Date fileDate = new Date();
        VendorFile file = new VendorFile("", "", "", fileDate, recallAccounts, summaryList);
        file.setUuid(uuid);

        inflightService.putFile(file);

        VendorFile result = inflightService.getFile(uuid);

        Assert.assertEquals(file.getUuid(), result.getUuid());
        Assert.assertEquals(file.getRecallAccountList().size(), 2);
    }

    @Ignore
    @Test
    public void filesByVendorAndDate() {
        InflightRepository inflightRepository = new InflightRepositoryImpl(dynamoDB);
        InflightService inflightService = new InflightServiceImpl(inflightRepository);

        String vendor = "VEND";
        String uuid = UUID.randomUUID().toString();
        Date fileDate = new Date();

        VendorFile file = new VendorFile(vendor, "", "", fileDate, Collections.emptyList(), Collections.emptyList());
        file.setUuid(uuid);
        inflightService.putFile(file);

        String secondUuid = UUID.randomUUID().toString();
        VendorFile secondFile = new VendorFile(vendor, "", "", fileDate, Collections.emptyList(), Collections.emptyList());
        secondFile.setUuid(secondUuid);
        inflightService.putFile(secondFile);

        List<VendorFile> files = inflightService.getFiles(vendor, fileDate);

        Assert.assertEquals(2, files.size());
    }

}
