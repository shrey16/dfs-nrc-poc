package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowStep;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class OutputLambdaTest {

    private SQSEvent event;
    private Context context;

    @Before
    public void setup() {
        SQSEvent.SQSMessage message = new SQSEvent.SQSMessage();
        WorkflowEvent workflowEvent = new WorkflowEvent();
        workflowEvent.setWorkflowStep(WorkflowStep.OUTPUT);
        message.setBody(WorkflowEventMapper.messageBodyFromDomain(workflowEvent));
        List<SQSEvent.SQSMessage> messages = new ArrayList<>();
        messages.add(message);
        event = new SQSEvent();

        event.setRecords(messages);
        context = mock(Context.class);
    }

    @Test
    public void handleRequest() {
    }

}
