package com.dfs.nrc.lambda;

import org.junit.Before;
import org.junit.Test;

public class DecodeLambdaTest {

    @Before
    public void setup() {
    }

    @Test
    public void validFileShouldInsertRecordAndGenerateSuccessMessage() {
    }

    @Test
    public void invalidFileSizeShouldPushFailureMsg() throws Exception {
        //    GIVEN a S3_BUCKET containing a [VENDOR] file with an incorrect file size
        //    AND SQS queues: WORKFLOW and DECODE
        //    WHEN the DECODE lambda gets triggered by a SQS message in the DECODE queue
        //    AND sends a [DECODE_FAILED] message to the WORKFLOW queue
    }

    @Test
    public void emptyFileShouldInsertEmptyRecordAndGenerateSuccessMessage() {
    }

    @Test
    public void invalidFormatShouldPushFailureMsg() throws Exception {
    }

    @Test
    public void incorrectDateShouldPushFailureMsg() throws Exception {
    }

    @Test
    public void missingFileShouldPushErrorMsg() {
    }

    @Test
    public void dbAccessErrorShouldPushErrorMsg() throws Exception {
    }

}
