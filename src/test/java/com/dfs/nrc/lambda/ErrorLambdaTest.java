package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.config.TestConfig;
import com.dfs.nrc.enums.WorkflowStep;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;

public class ErrorLambdaTest {
    private Context context;

    @Before
    public void setup() {
        context = mock(Context.class);
    }

    @Test
    public void errorLambdaLogsCorrectFormat() {
        //  GIVEN a [ERROR] message on the WORKFLOW queue
        Date date = new Date();
        String modifiedDate = new SimpleDateFormat("MMddyyy").format(date);
        String fileName = "VERFERROR" + modifiedDate + ".json";
        SQSEvent errorSQSEvent = TestConfig.createSQSEvent(fileName, WorkflowStep.ERROR, null);
        String errorMessage = String.format("ERROR file: [%s] step: [%s]", fileName, WorkflowStep.ERROR);

        //  WHEN the ERROR lambda gets triggered by a SQS message in the ERROR queue
        ErrorLambda lambda = new ErrorLambda();
        List<String> logMessages = lambda.handleRequest(errorSQSEvent, context);

        //  THEN the error message is formatted as expected
        for (String logMessage : logMessages) {
            Assert.assertEquals(errorMessage, logMessage);
        }
    }
}
