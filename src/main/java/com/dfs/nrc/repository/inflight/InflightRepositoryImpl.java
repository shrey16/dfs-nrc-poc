package com.dfs.nrc.repository.inflight;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.dfs.nrc.domain.Filter;
import com.dfs.nrc.domain.FilterExpression;
import com.dfs.nrc.domain.VendorFile;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class InflightRepositoryImpl implements InflightRepository {

    private final AmazonDynamoDB client;
    private final DynamoDBMapper mapper;

    public InflightRepositoryImpl() {
        this.client = AmazonDynamoDBClientBuilder.defaultClient();
        this.mapper = new DynamoDBMapper(this.client);
    }

    public InflightRepositoryImpl(AmazonDynamoDB client) {
        this.client = client;
        this.mapper = new DynamoDBMapper(this.client);
    }

    @Override
    public void putFile(VendorFile accountInfo) {
        this.mapper.save(accountInfo);
    }

    @Override
    public VendorFile getFile(@NotNull String uuid) {
        return mapper.load(VendorFile.class, uuid);
    }

    @Override
    public List<VendorFile> getFiles(List<Filter> filters) {
        FilterExpression filterExpression = new FilterExpression(filters);

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
            .withFilterExpression(filterExpression.getFilterExpression())
            .withExpressionAttributeNames(filterExpression.getAttributeNames())
            .withExpressionAttributeValues(filterExpression.getAttributeValues());

        return mapper.scan(VendorFile.class, scanExpression);
    }

}
