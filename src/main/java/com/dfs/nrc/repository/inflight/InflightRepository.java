package com.dfs.nrc.repository.inflight;

import com.dfs.nrc.domain.Filter;
import com.dfs.nrc.domain.VendorFile;

import java.util.List;

public interface InflightRepository {
    void putFile(VendorFile accountInfo);

    VendorFile getFile(String id);

    List<VendorFile> getFiles(List<Filter> filters);
}
