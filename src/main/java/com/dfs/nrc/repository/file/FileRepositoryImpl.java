package com.dfs.nrc.repository.file;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.dfs.nrc.config.AppConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FileRepositoryImpl implements FileRepository {
    private final String bucketName;
    private final AmazonS3 client;

    public FileRepositoryImpl() {
        this.client = AmazonS3ClientBuilder.defaultClient();
        this.bucketName = AppConfiguration.FILES_BUCKET;
    }

    public FileRepositoryImpl(AmazonS3 client, String bucketName) {
        this.client = client;
        this.bucketName = bucketName;
    }

    @Override
    @Nullable
    public S3Object getObject(@NotNull String key) {
        try {
            return client.getObject(new GetObjectRequest(bucketName, key));
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }

        return null;
    }
}
