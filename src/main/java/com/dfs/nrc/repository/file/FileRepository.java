package com.dfs.nrc.repository.file;

import com.amazonaws.services.s3.model.S3Object;

public interface FileRepository {
    S3Object getObject(String key);
}
