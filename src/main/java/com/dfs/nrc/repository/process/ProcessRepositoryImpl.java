package com.dfs.nrc.repository.process;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProcessRepositoryImpl implements ProcessRepository {
    private Connection connection;

    public ProcessRepositoryImpl() {
        String url = "jdbc:mariadb://dfsnrcpoc1.cp7tlp79g8xx.us-east-1.rds.amazonaws.com";

        try {
            this.connection = DriverManager.getConnection(url, "admin", "changeme");
        } catch (SQLException e) {
            System.out.println("Error Connecting to database");
            throw new RuntimeException(e);
        }
    }

    public ProcessRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ResultSet getItem(@NotNull String query) {
        query = "SELECT * FROM dfsnrcpoc.vendor";

        try {
            Statement stmt = connection.createStatement();
            return stmt.executeQuery(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
