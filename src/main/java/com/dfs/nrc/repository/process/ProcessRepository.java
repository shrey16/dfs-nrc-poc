package com.dfs.nrc.repository.process;

import java.sql.ResultSet;

public interface ProcessRepository {
    ResultSet getItem(String query);
}
