package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class TriggerLambda implements RequestHandler<S3Event, Void> {
    private static final Logger logger = LogManager.getLogger(TriggerLambda.class);
    private final SQSMessenger sqsMessenger;

    public TriggerLambda() {
        sqsMessenger = new SQSMessengerImpl(AppConfiguration.WORKFLOW_QUEUE);
    }

    public TriggerLambda(SQSMessenger sqsMessenger) {
        this.sqsMessenger = sqsMessenger;
    }

    @Override
    public Void handleRequest(final S3Event input, final Context context) {
        for (S3EventNotification.S3EventNotificationRecord record : input.getRecords()) {
            String fileName = record.getS3().getObject().getUrlDecodedKey();
            sqsMessenger.sendMessage(workflowEvent(fileName));
        }

        return null;
    }

    private WorkflowEvent workflowEvent(@NotNull String fileName) {
        WorkflowEvent event = new WorkflowEvent(fileName);

        // TODO: Re-enable once common is deployed
//        FileType fileType = FileUtil.fileType(fileName);
//        if (fileType == FileType.UNKNOWN_FILE) {
//            event.setWorkflowResult(WorkflowResult.FAILURE);
//        }

        return event;
    }
}

