package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import com.dfs.nrc.service.file.FileService;
import com.dfs.nrc.service.file.FileServiceImpl;
import com.dfs.nrc.service.inflight.InflightService;
import com.dfs.nrc.service.inflight.InflightServiceImpl;
import com.dfs.nrc.util.DecodeUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class DecodeLambda implements RequestHandler<SQSEvent, Void> {
    private static final Logger logger = LogManager.getLogger(DecodeLambda.class);

    private final SQSMessenger sqsMessenger;
    private final FileService fileService;
    private final InflightService inflightService;

    public DecodeLambda() {
        this.sqsMessenger = new SQSMessengerImpl(AppConfiguration.WORKFLOW_QUEUE);
        this.fileService = new FileServiceImpl();
        this.inflightService = new InflightServiceImpl();
    }

    public DecodeLambda(SQSMessenger sqsMessenger, FileService fileService, InflightService inflightService) {
        this.sqsMessenger = sqsMessenger;
        this.fileService = fileService;
        this.inflightService = inflightService;
    }

    @Override
    public Void handleRequest(@NotNull SQSEvent input, Context context) {
        for (SQSEvent.SQSMessage message : input.getRecords()) {
            WorkflowEvent workflowEvent = WorkflowEventMapper.domainFromMessageBody(message.getBody());

            try {
                // if create succeeds, do decode rules
                VendorFile file = fileService.createFile(workflowEvent);

                file.setDecodeErrors(DecodeUtil.decodeFile(file));
                if (!file.getDecodeErrors().isEmpty()) {
                    workflowEvent.setWorkflowResult(WorkflowResult.FAILURE);
                }

                // if decode rules succeed, save file to dynamo, send event to router
                inflightService.putFile(file);
            } catch (Exception e) {
                // if create of VendorFile fails (decoding failed)
                workflowEvent.setWorkflowResult(WorkflowResult.ERROR);
                // if create of file fails (decoding failed), handle
            }

            sqsMessenger.sendMessage(workflowEvent);
        }

        return null;
    }
}
