package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

public class ErrorLambda implements RequestHandler<SQSEvent, List<String>> {
    private static final Logger logger = LogManager.getLogger(ErrorLambda.class);

    @Override
    public List<String> handleRequest(@NotNull SQSEvent input, Context context) {
        return input.getRecords().stream().map(record -> {
            WorkflowEvent event = WorkflowEventMapper.domainFromMessageBody(record.getBody());
            String errorMessage = String.format("ERROR file: [%s] step: [%s]", event.getFileName(), event.getWorkflowStep());
            logger.info(errorMessage);
            return errorMessage;
        }).collect(Collectors.toList());
    }
}