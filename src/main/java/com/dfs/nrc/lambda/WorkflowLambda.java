package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import com.dfs.nrc.util.WorkflowProcessUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorkflowLambda implements RequestHandler<SQSEvent, Void> {
    private static final Logger logger = LogManager.getLogger(WorkflowLambda.class);

    private final SQSMessenger sqsMessenger;

    public WorkflowLambda() {
        this.sqsMessenger = new SQSMessengerImpl("");
    }

    public WorkflowLambda(SQSMessenger sqsMessenger) {
        this.sqsMessenger = sqsMessenger;
    }

    @Override
    public Void handleRequest(SQSEvent input, Context context) {
        for (SQSEvent.SQSMessage message : input.getRecords()) {
            WorkflowEvent workflowEvent = WorkflowEventMapper.domainFromMessageBody(message.getBody());
            logProcessResult(workflowEvent);
            processNextStep(workflowEvent);
        }
        return null;
    }

    private void processNextStep(WorkflowEvent workflowEvent) {
        workflowEvent.setWorkflowStep(WorkflowProcessUtil.nextStep(workflowEvent));
        String queue = WorkflowProcessUtil.queueName(workflowEvent);
        sqsMessenger.updateQueue(queue);
        sqsMessenger.sendMessage(workflowEvent);
    }

    // TODO: log result in SQL
    private void logProcessResult(WorkflowEvent event) {
        String logStatement = String.format("PROCESSED uuid: [%s] file: [%s] step: [%s] result: [%s]", event.getUuid(), event.getFileName(), event.getWorkflowStep(), event.getWorkflowResult());
        logger.info(logStatement);
    }
}
