package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import com.dfs.nrc.service.inflight.InflightService;
import com.dfs.nrc.service.inflight.InflightServiceImpl;
import com.dfs.nrc.util.ValidationUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class ValidateLambda implements RequestHandler<SQSEvent, Void> {
    private static final Logger logger = LogManager.getLogger(ValidateLambda.class);

    private final SQSMessenger sqsMessenger;
    private final InflightService inflightService;

    public ValidateLambda() {
        this.sqsMessenger = new SQSMessengerImpl(AppConfiguration.WORKFLOW_QUEUE);
        this.inflightService = new InflightServiceImpl();
    }

    public ValidateLambda(SQSMessenger sqsMessenger, InflightService inflightService) {
        this.sqsMessenger = sqsMessenger;
        this.inflightService = inflightService;
    }

    @Override
    public Void handleRequest(@NotNull SQSEvent input, Context context) {
        for (SQSEvent.SQSMessage message : input.getRecords()) {
            WorkflowEvent workflowEvent = WorkflowEventMapper.domainFromMessageBody(message.getBody());

            try {
                VendorFile file = inflightService.getFile(workflowEvent.getUuid());
//                List<Account> dm10Accounts = this.dm10Repository.accountsByVendorId(file.getVendorCode());
                file.setValidationErrors(ValidationUtil.validateFile(file));
                inflightService.putFile(file);
            } catch (Exception e) {
                workflowEvent.setWorkflowResult(WorkflowResult.FAILURE);
                e.printStackTrace();
            }

            sqsMessenger.sendMessage(workflowEvent);
        }

        return null;
    }
}
