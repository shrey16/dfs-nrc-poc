package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import com.dfs.nrc.service.process.ProcessService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class OutputLambda implements RequestHandler<SQSEvent, Void> {
    private static final Logger logger = LogManager.getLogger(OutputLambda.class);

    private final SQSMessenger sqsMessenger;
//    private final ProcessService processService;

    public OutputLambda() {
        this.sqsMessenger = new SQSMessengerImpl(AppConfiguration.WORKFLOW_QUEUE);
//        this.processService = new ProcessServiceImpl();
    }

    public OutputLambda(SQSMessenger sqsMessenger, ProcessService processService) {
        this.sqsMessenger = sqsMessenger;
//        this.processService = processService;
    }

    @Override
    public Void handleRequest(@NotNull SQSEvent input, Context context) {
        for (SQSEvent.SQSMessage message : input.getRecords()) {
            WorkflowEvent workflowEvent = WorkflowEventMapper.domainFromMessageBody(message.getBody());
            sqsMessenger.sendMessage(workflowEvent);
        }

        return null;
    }
}
