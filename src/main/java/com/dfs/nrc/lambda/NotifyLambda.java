package com.dfs.nrc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import com.dfs.nrc.messaging.email.EmailMessenger;
import com.dfs.nrc.messaging.email.EmailMessengerImpl;
import com.dfs.nrc.messaging.sqs.SQSMessenger;
import com.dfs.nrc.messaging.sqs.SQSMessengerImpl;
import com.dfs.nrc.service.inflight.InflightService;
import com.dfs.nrc.service.inflight.InflightServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class NotifyLambda implements RequestHandler<SQSEvent, Void> {
    private static final Logger logger = LogManager.getLogger(NotifyLambda.class);

    private final SQSMessenger sqsMessenger;
    private final InflightService inflightService;
    private final EmailMessenger emailMessenger;

    public NotifyLambda() {
        this.sqsMessenger = new SQSMessengerImpl(AppConfiguration.WORKFLOW_QUEUE);
        this.inflightService = new InflightServiceImpl();
        this.emailMessenger = new EmailMessengerImpl();
    }

    public NotifyLambda(SQSMessenger sqsMessenger, InflightService inflightService, EmailMessenger emailMessenger) {
        this.sqsMessenger = sqsMessenger;
        this.inflightService = inflightService;
        this.emailMessenger = emailMessenger;
    }

    @Override
    public Void handleRequest(@NotNull SQSEvent input, Context context) {
        for (SQSEvent.SQSMessage message : input.getRecords()) {
            WorkflowEvent event = WorkflowEventMapper.domainFromMessageBody(message.getBody());
            try {
                VendorFile vendorFile = inflightService.getFile(event.getUuid());
                // TODO: Set Recipients/CCs/BCCs based on vendorFile and WorkflowResult i.e EmailTemplateMapper
                this.emailMessenger.sendEmail(vendorFile, event);
            } catch (Exception e) {
                event.setWorkflowResult(WorkflowResult.ERROR);
                sqsMessenger.sendMessage(event);
            }

        }

        return null;
    }
}
