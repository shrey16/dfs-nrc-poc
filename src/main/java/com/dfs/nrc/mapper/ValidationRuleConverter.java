package com.dfs.nrc.mapper;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.dfs.nrc.enums.ValidationRule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ValidationRuleConverter implements DynamoDBTypeConverter<List<String>, List<ValidationRule>> {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public List<String> convert(List<ValidationRule> objects) {
        return objects.stream().map(object -> {
            try {
                return mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    @Override
    public List<ValidationRule> unconvert(List<String> objects) {
        return objects.stream().map(object -> {
            try {
                return mapper.readValue(object, ValidationRule.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }
}

