package com.dfs.nrc.mapper;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.dfs.nrc.enums.ProcessType;

public class ProcessTypeConverter implements DynamoDBTypeConverter<String, ProcessType> {
    @Override
    public String convert(ProcessType object) {
        return object.name();
    }

    @Override
    public ProcessType unconvert(String object) {
        return ProcessType.valueOf(object);
    }
}
