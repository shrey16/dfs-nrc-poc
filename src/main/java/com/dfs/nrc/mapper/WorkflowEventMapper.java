package com.dfs.nrc.mapper;

import com.dfs.nrc.domain.WorkflowEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class WorkflowEventMapper {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String messageBodyFromDomain(@NotNull WorkflowEvent workflowEvent) {
        try {
            return mapper.writeValueAsString(workflowEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static WorkflowEvent domainFromMessageBody(@NotNull String messageBody) {
        try {
            return mapper.readValue(messageBody, WorkflowEvent.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
