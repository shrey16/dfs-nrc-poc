package com.dfs.nrc.mapper;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.dfs.nrc.domain.AccountSummary;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class AccountSummaryConverter implements DynamoDBTypeConverter<List<String>, List<AccountSummary>> {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public List<String> convert(List<AccountSummary> objects) {
        return objects.stream().map(object -> {
            try {
                return mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    @Override
    public List<AccountSummary> unconvert(List<String> objects) {
        return objects.stream().map(object -> {
            try {
                return mapper.readValue(object, AccountSummary.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }
}
