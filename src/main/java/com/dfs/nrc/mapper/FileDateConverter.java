package com.dfs.nrc.mapper;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.dfs.nrc.config.AppConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDateConverter implements DynamoDBTypeConverter<String, Date> {
    private SimpleDateFormat dateFormat = new SimpleDateFormat(AppConfiguration.DATE_FORMAT);

    @Override
    public String convert(Date object) {
        return dateFormat.format(object);
    }

    @Override
    public Date unconvert(String object) {
        try {
            return dateFormat.parse(object);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
