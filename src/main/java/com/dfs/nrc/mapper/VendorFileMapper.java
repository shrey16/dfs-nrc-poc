package com.dfs.nrc.mapper;

import com.amazonaws.services.s3.model.S3Object;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;

public class VendorFileMapper {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static VendorFile fromS3Object(@NotNull WorkflowEvent event, @NotNull S3Object object) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(object.getObjectContent()))) {
            VendorFile file = mapper.readValue(input, VendorFile.class);
            file.setUuid(event.getUuid());
            file.setProcessType(event.getProcessType());
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toJsonString (@NotNull VendorFile vendorFile) {
        try {
            return mapper.writeValueAsString(vendorFile);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
