package com.dfs.nrc.messaging.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.mapper.WorkflowEventMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class SQSMessengerImpl implements SQSMessenger {
    private static final Logger logger = LogManager.getLogger(SQSMessengerImpl.class);

    private String queueName;
    private final AmazonSQS sqs;

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public SQSMessengerImpl(@NotNull final String queueName) {
        this.sqs = AmazonSQSClientBuilder.defaultClient();
        this.queueName = queueName;
    }

    public SQSMessengerImpl(@NotNull final String queueName, AmazonSQS sqs) {
        this.sqs = sqs;
        this.queueName = queueName;
    }

    @Override
    public String sendMessage(@NotNull final WorkflowEvent workflowEvent) {
        String myQueueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
        String messageBody = WorkflowEventMapper.messageBodyFromDomain(workflowEvent);
        SendMessageRequest request = new SendMessageRequest(myQueueUrl, messageBody);
        SendMessageResult sendResult = sqs.sendMessage(request);
        return String.format("Published message [%s], body: [%s]", sendResult.getMessageId(), messageBody);
    }

    @Override
    public void updateQueue(String queueName) {
        setQueueName(queueName);
    }
}
