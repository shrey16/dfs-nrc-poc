package com.dfs.nrc.messaging.sqs;

import com.dfs.nrc.domain.WorkflowEvent;

public interface SQSMessenger {
    String sendMessage(WorkflowEvent workflowEvent);
    void updateQueue(String queueName);
}
