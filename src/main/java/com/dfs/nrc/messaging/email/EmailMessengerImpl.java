package com.dfs.nrc.messaging.email;

import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.EmailMessage;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.service.template.TemplateService;
import com.dfs.nrc.service.template.TemplateServiceImpl;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

// TODO: Integrate with DFS SMTP (No Auth)
public class EmailMessengerImpl implements EmailMessenger {

    private final TemplateService templateService;
    private static String hostName;
    private Properties props;
    private EmailMessage message;

    public EmailMessengerImpl() {
        this.hostName = "smtp.gmail.com";

        this.props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", this.hostName);
        props.put("mail.smtp.port", "465");

        // SSL Factory
        props.put("mail.smtp.socketFactory.class",
            "javax.net.ssl.SSLSocketFactory");
        this.templateService = new TemplateServiceImpl();
    }

    private Message createMessage(Session session, VendorFile vendorFile, WorkflowEvent event) throws Exception {
        this.message = new EmailMessage("noreply@discover.com","david.durkin@slalom.com", "Verification File Error");
        this.message.setContent(this.templateService.generateEmailMessageFromProperTemplate(vendorFile, event));
        Message message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(this.message.getFrom()));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(this.message.getTo()));
            message.setSubject(this.message.getSubject());
            message.setText(this.message.getContent());
        } catch (Exception e) {
            throw (e);
        }

        return message;
    }

    @Override
    public void sendEmail(VendorFile vendorFile, WorkflowEvent event) {
        Authenticator auth = new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(AppConfiguration.GMAIL_EMAIL, AppConfiguration.GMAIL_PASSWORD);
            }
        };

        Session session = Session.getDefaultInstance(this.props, auth);
        session.setDebug(true);
        try {
            Message message = createMessage(session, vendorFile, event);
            System.out.println("Sending message...");
            Transport.send(message);
            System.out.println("Sent!");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
