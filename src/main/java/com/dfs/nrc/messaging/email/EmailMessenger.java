package com.dfs.nrc.messaging.email;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;

public interface EmailMessenger {
    void sendEmail(VendorFile vendorFile, WorkflowEvent event);
}
