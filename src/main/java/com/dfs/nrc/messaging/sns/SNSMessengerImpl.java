package com.dfs.nrc.messaging.sns;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.dfs.nrc.enums.SNSTopic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class SNSMessengerImpl implements SNSMessenger {
    private static final Logger logger = LogManager.getLogger(SNSMessengerImpl.class);
    private final AmazonSNS sns;

    public SNSMessengerImpl() {
        this.sns = AmazonSNSClientBuilder.defaultClient();
    }

    public SNSMessengerImpl(AmazonSNS sns) {
        this.sns = sns;
    }

    @Override
    public void sendMessage(@NotNull final SNSTopic topic, @NotNull final String message) {
        final String serviceTopicArn = sns.createTopic(new CreateTopicRequest(topic.toString())).getTopicArn();

        PublishResult publishResult = sns.publish(serviceTopicArn, message);
        logger.info(String.format("Published message [%s] to topic [%s]", publishResult.getMessageId(), serviceTopicArn));
    }
}
