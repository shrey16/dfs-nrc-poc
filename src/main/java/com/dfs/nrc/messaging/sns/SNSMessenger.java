package com.dfs.nrc.messaging.sns;

import com.dfs.nrc.enums.SNSTopic;

public interface SNSMessenger {
    void sendMessage(final SNSTopic topic, final String message);
}
