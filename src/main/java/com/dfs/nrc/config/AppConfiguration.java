package com.dfs.nrc.config;

public class AppConfiguration {
    public static final String DATE_FORMAT = "MMddyyyy";
    public static final String FILES_BUCKET = "dfs-nrc-data-bucket";
    public static final String EMAIL_TEMPLATES_BUCKET = "dfs-nrc-email-templates";
    public static final String INFLIGHT_TABLE = "dfs-nrc-in-flight-data";

    public static final String WORKFLOW_QUEUE = "dfs-nrc-result-queue";
    public static final String DECODE_QUEUE = "dfs-nrc-decode-queue";
    public static final String ERROR_QUEUE = "dfs-nrc-error-queue";
    public static final String NOTIFY_QUEUE = "dfs-nrc-notifier-queue";
    public static final String OUTPUT_QUEUE = "dfs-nrc-output-queue";
    public static final String VALIDATE_QUEUE = "dfs-nrc-validate-queue";
    // TODO: Remove These... Testing/Demo Purposes Only
    public static final String GMAIL_EMAIL = "";
    public static final String GMAIL_PASSWORD = "";
}
