package com.dfs.nrc.domain;

public class Filter {

    private String attributeName;

    private String attributeValue;

    private Operation operation;

    private AttributeType attributeType;

    public enum Operation {
        EQ, GE, LE, GT, LT, CONTAINS, STARTS_WITH
    }

    public enum AttributeType {
        STRING, NUMBER
    }

    public Filter(String attributeName, String attributeValue, Operation operation, AttributeType attributeType) {
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
        this.operation = operation;
        this.attributeType = attributeType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public Operation getOperation() {
        return operation;
    }
}
