package com.dfs.nrc.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.enums.DecodeRule;
import com.dfs.nrc.enums.ProcessType;
import com.dfs.nrc.enums.ValidationRule;
import com.dfs.nrc.mapper.AccountSummaryConverter;
import com.dfs.nrc.mapper.FileDateConverter;
import com.dfs.nrc.mapper.DecodeRuleConverter;
import com.dfs.nrc.mapper.ProcessTypeConverter;
import com.dfs.nrc.mapper.RecallAccountConverter;
import com.dfs.nrc.mapper.ValidationRuleConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties({"uuid", "processType"})
@DynamoDBTable(tableName = AppConfiguration.INFLIGHT_TABLE)
public class VendorFile {
    @DynamoDBHashKey
    private String uuid;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = ProcessTypeConverter.class)
    private ProcessType processType = ProcessType.AIF_MNT;

    @DynamoDBAttribute
    private String vendorCode;

    @DynamoDBAttribute
    private String fileName;

    @DynamoDBAttribute
    private String fileExtension;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = FileDateConverter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConfiguration.DATE_FORMAT)
    private Date fileDate;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = RecallAccountConverter.class)
    private List<RecallAccount> recallAccountList = Collections.emptyList();

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = AccountSummaryConverter.class)
    private List<AccountSummary> summaryList = Collections.emptyList();

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = ValidationRuleConverter.class)
    private List<ValidationRule> validationErrors = Collections.emptyList();

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = DecodeRuleConverter.class)
    private List<DecodeRule> decodeErrors = Collections.emptyList();

    public VendorFile() {
    }

    public VendorFile(
        @NotNull final String vendorCode,
        @NotNull final String fileName,
        @NotNull final String fileExtension,
        @NotNull final Date fileDate,
        @NotNull final List<RecallAccount> recallAccountList,
        @NotNull final List<AccountSummary> summaryList
    ) {
        this.vendorCode = vendorCode;
        this.fileName = fileName;
        this.fileExtension = fileExtension;
        this.fileDate = fileDate;
        this.recallAccountList = recallAccountList;
        this.summaryList = summaryList;
    }

    public String getUuid() {
        return uuid;
    }

    public String getFileName() {
        return fileName;
    }

    public List<RecallAccount> getRecallAccountList() {
        return recallAccountList;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public List<ValidationRule> getValidationErrors() {
        return validationErrors;
    }

    public List<AccountSummary> getSummaryList() {
        return summaryList;
    }

    public ProcessType getProcessType() {
        return processType;
    }

    public void setProcessType(ProcessType processType) {
        this.processType = processType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public void setValidationErrors(List<ValidationRule> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public void setDecodeErrors(List<DecodeRule> decodeErrors) { this.decodeErrors = decodeErrors; }

    public List<DecodeRule> getDecodeErrors() { return this.decodeErrors; }

    @DynamoDBIgnore
    public List<String> getDecodeErrorStrings() {
        return  this.getDecodeErrors().stream().map(error -> {
            switch (error) {
                case FILE_NAME:
                    return "[FILE DECODE ERROR] - Incorrect File Name";
                case FILE_DATE:
                    return "[FILE DECODE ERROR] - Incorrect File Date";
                case FILE_FORMAT:
                    return "[FILE DECODE ERROR] - Incorrect File Format";
            }
            return "[ERROR] - Unknown";
        }).collect(Collectors.toList());
    }

    public void setRecallAccountList(List<RecallAccount> recallAccountList) {
        this.recallAccountList = recallAccountList;
    }

    public void setSummaryList(List<AccountSummary> summaryList) {
        this.summaryList = summaryList;
    }
}
