package com.dfs.nrc.domain;

import com.dfs.nrc.config.AppConfiguration;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecallAccount {
    private String accountNumber;

    private String workGroup;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConfiguration.DATE_FORMAT)
    private Date fileDate;

    public RecallAccount() {
    }

    public RecallAccount(
        @NotNull final String accountNumber,
        @NotNull final String workGroup,
        @NotNull final Date fileDate
    ) {
        this.accountNumber = accountNumber;
        this.workGroup = workGroup;
        this.fileDate = fileDate;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setWorkGroup(String workGroup) {
        this.workGroup = workGroup;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getWorkGroup() {
        return workGroup;
    }

    public Date getFileDate() {
        return fileDate;
    }
}

