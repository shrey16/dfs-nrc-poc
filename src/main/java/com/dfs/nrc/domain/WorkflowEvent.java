package com.dfs.nrc.domain;

import com.dfs.nrc.enums.ProcessType;
import com.dfs.nrc.enums.WorkflowResult;
import com.dfs.nrc.enums.WorkflowStep;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class WorkflowEvent {
    private String uuid;
    private String fileName;
    private ProcessType processType = ProcessType.AIF_MNT;
    private WorkflowStep workflowStep = WorkflowStep.TRIGGER;
    private WorkflowResult workflowResult = WorkflowResult.SUCCESS;

    public WorkflowEvent() {
    }

    public WorkflowEvent(@NotNull final String fileName) {
        this.uuid = UUID.randomUUID().toString();
        this.fileName = fileName;
    }

    public String getUuid() {
        return uuid;
    }

    public String getFileName() {
        return fileName;
    }

    public WorkflowStep getWorkflowStep() { return workflowStep; }

    public WorkflowResult getWorkflowResult() { return workflowResult; }

    public ProcessType getProcessType() {
        return processType;
    }

    public void setWorkflowStep(WorkflowStep workflowStep) {
        this.workflowStep = workflowStep;
    }

    public void setWorkflowResult(WorkflowResult workflowResult) {
        this.workflowResult = workflowResult;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
