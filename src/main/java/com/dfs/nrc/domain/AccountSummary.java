package com.dfs.nrc.domain;

import com.dfs.nrc.config.AppConfiguration;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountSummary {
    private String vendorName;

    private String personVerifying;

    private String phone;

    private String email;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConfiguration.DATE_FORMAT)
    private Date todaysDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConfiguration.DATE_FORMAT)
    private Date fileDate;

    private String workGroup;

    private Integer totalAccountsRecalled;

    private Integer totalNewAccountsPlaced;

    private Double totalBalanceNewAccounts;

    private Integer totalAccountsReassigned;

    private Double totalBalanceReassigned;

    public AccountSummary() {
    }

    public AccountSummary(
        @NotNull final String vendorName,
        @NotNull final String personVerifying,
        @NotNull final String phone,
        @NotNull final String email,
        @NotNull final Date todaysDate,
        @NotNull final Date fileDate,
        @NotNull final String workGroup,
        @NotNull final Integer totalAccountsRecalled,
        @NotNull final Integer totalNewAccountsPlaced,
        @NotNull final Double totalBalanceNewAccounts,
        @NotNull final Integer totalAccountsReassigned,
        @NotNull final Double totalBalanceReassigned
    ) {
        this.vendorName = vendorName;
        this.personVerifying = personVerifying;
        this.phone = phone;
        this.email = email;
        this.todaysDate = todaysDate;
        this.fileDate = fileDate;
        this.workGroup = workGroup;
        this.totalAccountsRecalled = totalAccountsRecalled;
        this.totalNewAccountsPlaced = totalNewAccountsPlaced;
        this.totalBalanceNewAccounts = totalBalanceNewAccounts;
        this.totalAccountsReassigned = totalAccountsReassigned;
        this.totalBalanceReassigned = totalBalanceReassigned;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public void setPersonVerifying(String personVerifying) {
        this.personVerifying = personVerifying;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTodaysDate(Date todaysDate) {
        this.todaysDate = todaysDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public void setWorkGroup(String workGroup) {
        this.workGroup = workGroup;
    }

    public void setTotalAccountsRecalled(Integer totalAccountsRecalled) {
        this.totalAccountsRecalled = totalAccountsRecalled;
    }

    public void setTotalNewAccountsPlaced(Integer totalNewAccountsPlaced) {
        this.totalNewAccountsPlaced = totalNewAccountsPlaced;
    }

    public void setTotalBalanceNewAccounts(Double totalBalanceNewAccounts) {
        this.totalBalanceNewAccounts = totalBalanceNewAccounts;
    }

    public void setTotalAccountsReassigned(Integer totalAccountsReassigned) {
        this.totalAccountsReassigned = totalAccountsReassigned;
    }

    public void setTotalBalanceReassigned(Double totalBalanceReassigned) {
        this.totalBalanceReassigned = totalBalanceReassigned;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getPersonVerifying() {
        return personVerifying;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Date getTodaysDate() {
        return todaysDate;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public String getWorkGroup() {
        return workGroup;
    }

    public Integer getTotalAccountsRecalled() {
        return totalAccountsRecalled;
    }

    public Integer getTotalNewAccountsPlaced() {
        return totalNewAccountsPlaced;
    }

    public Double getTotalBalanceNewAccounts() {
        return totalBalanceNewAccounts;
    }

    public Integer getTotalAccountsReassigned() {
        return totalAccountsReassigned;
    }

    public Double getTotalBalanceReassigned() {
        return totalBalanceReassigned;
    }
}

