package com.dfs.nrc.service.process;

import com.dfs.nrc.domain.ProcessItem;
import com.dfs.nrc.mapper.ProcessItemMapper;
import com.dfs.nrc.repository.process.ProcessRepository;
import com.dfs.nrc.repository.process.ProcessRepositoryImpl;
import org.jetbrains.annotations.NotNull;

public class ProcessServiceImpl implements ProcessService {
    private final ProcessRepository repository;

    public ProcessServiceImpl() {
        this.repository = new ProcessRepositoryImpl();
    }

    public ProcessServiceImpl(ProcessRepository repository) {
        this.repository = repository;
    }

    @Override
    public ProcessItem get(@NotNull String query) {
        return ProcessItemMapper.itemFromDB(repository.getItem(query));
    }
}
