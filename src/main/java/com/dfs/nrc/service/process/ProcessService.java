package com.dfs.nrc.service.process;

import com.dfs.nrc.domain.ProcessItem;

public interface ProcessService {
    ProcessItem get(String query);
}
