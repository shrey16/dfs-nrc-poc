package com.dfs.nrc.service.file;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.mapper.VendorFileMapper;
import com.dfs.nrc.repository.file.FileRepository;
import com.dfs.nrc.repository.file.FileRepositoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FileServiceImpl implements FileService {
    private final FileRepository repository;

    public FileServiceImpl() {
        repository = new FileRepositoryImpl();
    }

    public FileServiceImpl(@NotNull FileRepository repository) {
        this.repository = repository;
    }

    @Override
    @Nullable
    public VendorFile createFile(@NotNull WorkflowEvent event) throws Exception {
        S3Object object = repository.getObject(event.getFileName());
        return VendorFileMapper.fromS3Object(event, object);
    }
}
