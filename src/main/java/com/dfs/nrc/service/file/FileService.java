package com.dfs.nrc.service.file;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;

public interface FileService {
    VendorFile createFile(WorkflowEvent event) throws Exception;
}
