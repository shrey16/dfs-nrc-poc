package com.dfs.nrc.service.inflight;

import com.dfs.nrc.domain.VendorFile;

import java.util.Date;
import java.util.List;

public interface InflightService {
    VendorFile getFile(String uuid);

    List<VendorFile> getFiles(String vendor, Date date);

    void putFile(VendorFile info);
}
