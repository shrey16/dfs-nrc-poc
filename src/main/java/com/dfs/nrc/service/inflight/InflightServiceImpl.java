package com.dfs.nrc.service.inflight;

import com.dfs.nrc.domain.Filter;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.mapper.DateUtil;
import com.dfs.nrc.repository.inflight.InflightRepository;
import com.dfs.nrc.repository.inflight.InflightRepositoryImpl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class InflightServiceImpl implements InflightService {
    private InflightRepository repository;

    public InflightServiceImpl() {
        this.repository = new InflightRepositoryImpl();
    }

    public InflightServiceImpl(InflightRepository repository) {
        this.repository = repository;
    }

    @Override
    public VendorFile getFile(String uuid) {
        return repository.getFile(uuid);
    }

    @Override
    public List<VendorFile> getFiles(String vendor, Date date) {
        Filter vendorCodeFilter = new Filter("vendorCode", vendor, Filter.Operation.EQ, Filter.AttributeType.STRING);
        Filter fileDateFilter = new Filter("fileDate", DateUtil.stringFromDate(date), Filter.Operation.EQ, Filter.AttributeType.STRING);
        List<Filter> filters = Arrays.asList(vendorCodeFilter, fileDateFilter);

        return repository.getFiles(filters);
    }

    @Override
    public void putFile(VendorFile accountInfo) {
        repository.putFile(accountInfo);
    }
}
