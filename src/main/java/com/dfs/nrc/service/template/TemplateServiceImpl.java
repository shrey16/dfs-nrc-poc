package com.dfs.nrc.service.template;

import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.repository.file.FileRepository;
import com.dfs.nrc.repository.file.FileRepositoryImpl;
import com.dfs.nrc.util.TemplateUtil;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class TemplateServiceImpl implements TemplateService {
    private final FileRepository repository;
    private Map<String, Object> root = new HashMap<>();
    private static Configuration templateConfig = new Configuration(Configuration.VERSION_2_3_28);
    private Template emailTemplate;

    public TemplateServiceImpl() {
        this.repository = new FileRepositoryImpl(AmazonS3ClientBuilder.defaultClient(), AppConfiguration.EMAIL_TEMPLATES_BUCKET);
    }

    public TemplateServiceImpl(@NotNull FileRepository repository) {
        this.repository = repository;
    }

    @Override
    public String generateEmailMessageFromProperTemplate(VendorFile vendorFile, WorkflowEvent workflowEvent) {
        String templateName = TemplateUtil.selectTemplateName(vendorFile, workflowEvent);
        return createStringFromS3Template(templateName, vendorFile);
    }

    private String getTemplateStringFromS3(String templateName) {
        S3Object object = repository.getObject(templateName);
        S3ObjectInputStream stream = object.getObjectContent();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        try {
            return IOUtils.toString(stream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String createStringFromS3Template(String templateName, VendorFile vendorFile) {
        this.root.put("vendorFile", vendorFile);
        StringWriter stringWriter = new StringWriter();
        String content;
        try {
            String templateString = this.getTemplateStringFromS3(templateName);
            StringTemplateLoader stringLoader = new StringTemplateLoader();
            stringLoader.putTemplate(templateName, templateString);
            this.templateConfig.setTemplateLoader(stringLoader);
            this.emailTemplate = templateConfig.getTemplate(templateName);
            emailTemplate.process(root, stringWriter);
            content = stringWriter.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return content;
    }
}
