package com.dfs.nrc.service.template;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;

public interface TemplateService {
    String generateEmailMessageFromProperTemplate(VendorFile vendorFile, WorkflowEvent workflowEvent);
}
