package com.dfs.nrc.util;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.enums.ValidationRule;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ValidationUtil {
    private final static List<ValidationRule> AIF_MNT_RULES = Arrays.asList(
            ValidationRule.FILE_NAME,
            ValidationRule.ACCOUNTS
    );

    public static List<ValidationRule> getRules(@NotNull VendorFile file) {
        switch (file.getProcessType()) {
            default: return AIF_MNT_RULES;
        }
    }

    public static List<ValidationRule> validateFile(@NotNull VendorFile file) {
        List<ValidationRule> rules = getRules(file);

        return rules.stream()
                .map(rule -> validationResult(file, rule))
                .distinct()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static ValidationRule validationResult(@NotNull VendorFile file, @NotNull ValidationRule rule) {
        switch (rule) {
            case FILE_NAME: {
                if (file.getFileName().isEmpty()) {
                    return ValidationRule.FILE_NAME;
                }
            }
            case ACCOUNTS: {
                if (file.getSummaryList().size() != file.getRecallAccountList().size()) {
                    return ValidationRule.ACCOUNTS;
                }
            }
            default: {
                return null;
            }
        }
    }
}
