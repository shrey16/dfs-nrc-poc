package com.dfs.nrc.util;

import com.dfs.nrc.config.AppConfiguration;
import com.dfs.nrc.domain.WorkflowEvent;
import com.dfs.nrc.enums.WorkflowStep;
import org.jetbrains.annotations.NotNull;

public class WorkflowProcessUtil {
    public static WorkflowStep nextStep(@NotNull WorkflowEvent workflowEvent) {
        switch (workflowEvent.getWorkflowResult()) {
            case SUCCESS:
                switch (workflowEvent.getWorkflowStep()) {
                    case TRIGGER:
                        return WorkflowStep.DECODE;
                    case DECODE:
                        return WorkflowStep.VALIDATE;
                    case VALIDATE:
                        return WorkflowStep.OUTPUT;
                    case OUTPUT:
                        return WorkflowStep.NOTIFY;
                    default:
                        return WorkflowStep.ERROR;
                }
            case FAILURE:
                return WorkflowStep.NOTIFY;
            default:
                return WorkflowStep.ERROR;
        }
    }

    public static String queueName(@NotNull WorkflowEvent workflowEvent) {
        switch (workflowEvent.getWorkflowResult()) {
            case SUCCESS:
                switch (workflowEvent.getWorkflowStep()) {
                    case DECODE:
                        return AppConfiguration.DECODE_QUEUE;
                    case VALIDATE:
                        return AppConfiguration.VALIDATE_QUEUE;
                    case NOTIFY:
                        return AppConfiguration.NOTIFY_QUEUE;
                    case OUTPUT:
                        return AppConfiguration.OUTPUT_QUEUE;
                    default:
                        return AppConfiguration.ERROR_QUEUE;
                }
            case FAILURE:
                return AppConfiguration.NOTIFY_QUEUE;
            default:
                return AppConfiguration.ERROR_QUEUE;
        }
    }
}
