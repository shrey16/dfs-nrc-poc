package com.dfs.nrc.util;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.domain.WorkflowEvent;

public class TemplateUtil {
    private static String FILENAME_FAILURE_1 = "verf_failure_filename_1.txt";
    private static String VERF_SUCCESS = "verf_success.txt";

    public static String selectTemplateName(VendorFile vendorFile, WorkflowEvent workflowEvent) {
        switch(workflowEvent.getWorkflowResult()) {
            case SUCCESS:
                return VERF_SUCCESS;
            case FAILURE:
                return FILENAME_FAILURE_1;
                // TODO: Verification Failure Notification Logic
            case ERROR:
                // placeholder
                // TODO add more templates
                return VERF_SUCCESS;
            default:
                return null;
        }
    }
}
