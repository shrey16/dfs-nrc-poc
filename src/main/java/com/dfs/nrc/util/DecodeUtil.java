package com.dfs.nrc.util;

import com.dfs.nrc.domain.VendorFile;
import com.dfs.nrc.enums.DecodeRule;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DecodeUtil {
    private final static List<DecodeRule> AIF_MNT_RULES = Arrays.asList(
            DecodeRule.FILE_NAME,
            DecodeRule.FILE_DATE
    );

    public static List<DecodeRule> getRules(@NotNull VendorFile file) {
        switch (file.getProcessType()) {
            default: return AIF_MNT_RULES;
        }
    }

    public static List<DecodeRule> decodeFile(@NotNull VendorFile file) {
        List<DecodeRule> rules = getRules(file);

        return rules.stream()
                .map(rule -> decodeResult(file, rule))
                .distinct()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static DecodeRule decodeResult(@NotNull VendorFile file, @NotNull DecodeRule rule) {
        switch (rule) {
            case FILE_NAME: {
                // TODO: Re-enable once common is deployed
//                if (!FileUtil.isFileNameValid(file.getFileName())) {
//                    return DecodeRule.FILE_NAME;
//                }
            }
            case FILE_DATE: {
                // TODO: Re-enable once common is deployed
//                if (FileUtil.isFileNameValid(file.getFileName()) && !isFileNameToday(file.getFileName())) {
//                    return DecodeRule.FILE_DATE;
//                }
            }
            default: {
                return null;
            }
        }
    }

    private static boolean isFileNameToday(@NotNull String fileName) {
        Calendar today = Calendar.getInstance();
        Calendar fileDate = Calendar.getInstance();
        // TODO: Re-enable once common is deployed
//        fileDate.setTime(FileUtil.fileDate(fileName));
        return today.get(Calendar.YEAR) == fileDate.get(Calendar.YEAR) &&
                today.get(Calendar.DAY_OF_YEAR) == fileDate.get(Calendar.DAY_OF_YEAR);
    }
}
