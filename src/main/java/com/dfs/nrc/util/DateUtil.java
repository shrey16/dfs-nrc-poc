package com.dfs.nrc.mapper;

import com.dfs.nrc.config.AppConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat(AppConfiguration.DATE_FORMAT);

    public static String stringFromDate(Date object) {
        return dateFormat.format(object);
    }

    public static Date dateFromString(String object) {
        try {
            return dateFormat.parse(object);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

}
