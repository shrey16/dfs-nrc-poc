package com.dfs.nrc.enums;

public enum WorkflowStep {
    TRIGGER,
    DECODE,
    NOTIFY,
    OUTPUT,
    VALIDATE,
    ERROR
}