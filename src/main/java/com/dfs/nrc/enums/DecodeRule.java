package com.dfs.nrc.enums;

public enum DecodeRule {
    NONE,
    FILE_NAME,
    FILE_FORMAT,
    FILE_DATE
}
