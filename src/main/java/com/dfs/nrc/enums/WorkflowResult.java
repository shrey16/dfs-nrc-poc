package com.dfs.nrc.enums;

public enum WorkflowResult {
    SUCCESS,
    FAILURE,
    ERROR
}
